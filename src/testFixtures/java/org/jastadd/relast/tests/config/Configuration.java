package org.jastadd.relast.tests.config;

public class Configuration {

  private String name;
  private String[] args = new String[]{};
  private boolean fail = false;
  private String[] errMatches = new String[]{};
  private String[] errContains = new String[]{};
  private String[] outMatches = new String[]{};
  private String[] outContains = new String[]{};
  private String in = "in";
  private String out = "out";
  private String expected = "expected";

  private boolean compare = false;

  public String getIn() {
    return in;
  }

  public void setIn(String in) {
    this.in = in;
  }

  public String getOut() {
    return out;
  }

  public void setOut(String out) {
    this.out = out;
  }

  public String getExpected() {
    return expected;
  }

  public void setExpected(String expected) {
    this.expected = expected;
  }

  @com.fasterxml.jackson.annotation.JsonGetter("out-matches")
  public String[] getOutMatches() {
    return outMatches;
  }

  @com.fasterxml.jackson.annotation.JsonSetter("out-matches")
  public void setOutMatches(String[] outMatches) {
    this.outMatches = outMatches;
  }

  @com.fasterxml.jackson.annotation.JsonGetter("out-contains")
  public String[] getOutContains() {
    return outContains;
  }

  @com.fasterxml.jackson.annotation.JsonSetter("out-contains")
  public void setOutContains(String[] outContains) {
    this.outContains = outContains;
  }

  @com.fasterxml.jackson.annotation.JsonGetter("err-matches")
  public String[] getErrMatches() {
    return errMatches;
  }

  @com.fasterxml.jackson.annotation.JsonSetter("err-matches")
  public void setErrMatches(String[] errMatches) {
    this.errMatches = errMatches;
  }

  @com.fasterxml.jackson.annotation.JsonGetter("err-contains")
  public String[] getErrContains() {
    return errContains;
  }

  @com.fasterxml.jackson.annotation.JsonSetter("err-contains")
  public void setErrContains(String[] errContains) {
    this.errContains = errContains;
  }

  public String getName() {
    return name;
  }

  public void setName(String name) {
    this.name = name;
  }

  public String[] getArgs() {
    return args;
  }

  public void setArgs(String[] args) {
    this.args = args;
  }

  @com.fasterxml.jackson.annotation.JsonGetter("fail")
  public boolean shouldFail() {
    return fail;
  }

  @com.fasterxml.jackson.annotation.JsonSetter("fail")
  public void shouldFail(boolean fail) {
    this.fail = fail;
  }

  @com.fasterxml.jackson.annotation.JsonGetter("compare")
  public boolean shouldCompare() {
    return compare;
  }

  @com.fasterxml.jackson.annotation.JsonSetter("compare")
  public void shouldCompare(boolean compare) {
    this.compare = compare;
  }
}
