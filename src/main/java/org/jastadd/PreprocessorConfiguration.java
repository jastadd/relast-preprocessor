/* Copyright (c) 2013-2015, Jesper Öqvist <jesper.oqvist@cs.lth.se>
 * All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without
 * modification, are permitted provided that the following conditions are met:
 *
 *     * Redistributions of source code must retain the above copyright notice,
 *       this list of conditions and the following disclaimer.
 *     * Redistributions in binary form must reproduce the above copyright
 *       notice, this list of conditions and the following disclaimer in the
 *       documentation and/or other materials provided with the distribution.
 *     * Neither the name of the Lund University nor the names of its
 *       contributors may be used to endorse or promote products derived from
 *       this software without specific prior written permission.
 *
 * THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS "AS IS"
 * AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO, THE
 * IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE
 * LIABLE FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR
 * CONSEQUENTIAL DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF
 * SUBSTITUTE GOODS OR SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS
 * INTERRUPTION) HOWEVER CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN
 * CONTRACT, STRICT LIABILITY, OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE)
 * ARISING IN ANY WAY OUT OF THE USE OF THIS SOFTWARE, EVEN IF ADVISED OF THE
 * POSSIBILITY OF SUCH DAMAGE.
 */
package org.jastadd;

import org.jastadd.option.ArgumentParser;
import org.jastadd.option.FlagOption;
import org.jastadd.option.Option;

import java.io.PrintStream;
import java.util.*;

/**
 * Tracks JastAdd configuration options.
 *
 * @author Jesper Öqvist <jesper.oqvist@cs.lth.se>
 */
public class PreprocessorConfiguration extends org.jastadd.Configuration {

  /**
   * Indicates if there were unknown command-line options
   */
  final boolean unknownOptions;
  private final Map<String, Option<?>> options = new HashMap<>();
  private final boolean isJastAddCompliant;
  private final ArgumentParser argParser;
  /**
   * Parse options from an argument list.
   *
   * @param args Command-line arguments to build configuration from
   * @param err  output stream to print configuration warnings to
   */
  public PreprocessorConfiguration(String[] args, PrintStream err, boolean isJastAddCompliant, Collection<Option<?>> extraOptions) {
    argParser = new ArgumentParser();
    this.isJastAddCompliant = isJastAddCompliant;

    if (isJastAddCompliant) {
      Collection<Option<?>> jastAddOptions = allJastAddOptions();
      for (Option<?> o : jastAddOptions) {
        options.put(o.name(), o);
      }
      argParser.addOptions(jastAddOptions);
    }

    // if the JastAdd options are supported, we have to check for duplicates!
    for (Option option : extraOptions) {
      if (options.containsKey(option.name())) {
        System.err.println("Unable to add option '" + option.name() + "', because there is a JastAdd option with the same name.");
      } else {
        if (option.name().equals("help") && option instanceof FlagOption) {
          this.helpOption = (FlagOption) option;
        } else if (option.name().equals("version") && option instanceof FlagOption) {
          this.versionOption = (FlagOption) option;
        }
        argParser.addOption(option);
        options.put(option.name(), option);
      }
    }

    unknownOptions = !argParser.parseArgs(args, err);
    filenames = argParser.getFilenames();
  }

  public ArgumentParser getArgParser() {
    return argParser;
  }

  public Optional<Option> getOption(String name) {
    return options.containsKey(name) ? Optional.of(options.get(name)) : Optional.empty();
  }

  public boolean isJastAddCompliant() {
    return isJastAddCompliant;
  }

  /**
   * Print help
   *
   * @param out Output stream to print help to.
   */
  @Override
  public void printHelp(PrintStream out) {
    out.println("This program reads a number of .jrag, .jadd, and .ast files");
    out.println("Options:");
    argParser.printHelp(out);
    out.println();
    out.println("Arguments:");
    out.println("  Names of abstract grammr (.ast) and aspect (.jrag and .jadd) files.");
  }

  /**
   * @return all files
   */
  @Override
  public Collection<String> getFiles() {
    return Collections.unmodifiableCollection(filenames);
  }

  private Collection<Option<?>> allJastAddOptions() {
    Collection<Option<?>> allOptions = new LinkedList<>();
    allOptions.add(ASTNodeOption);
    allOptions.add(ListOption);
    allOptions.add(OptOption);
    allOptions.add(jjtreeOption);
    allOptions.add(grammarOption);
    allOptions.add(generateAnnotations);
    allOptions.add(defaultMapOption);
    allOptions.add(defaultSetOption);
    allOptions.add(lazyMapsOption);
    allOptions.add(privateOption);
    allOptions.add(rewriteOption);
    allOptions.add(beaverOption);
    allOptions.add(lineColumnNumbersOption);
    allOptions.add(visitCheckOption);
    allOptions.add(traceVisitCheckOption);
    allOptions.add(cacheCycleOption);
    allOptions.add(componentCheckOption);
    allOptions.add(inhEqCheckOption);
    allOptions.add(refineLegacyOption);
    allOptions.add(licenseOption);
    allOptions.add(debugOption);
    allOptions.add(outputDirOption);
    allOptions.add(staticStateOption);
    allOptions.add(tracingOption);
    allOptions.add(flushOption);
    allOptions.add(packageNameOption);
    allOptions.add(versionOption);
    allOptions.add(helpOption);
    allOptions.add(printNonStandardOptionsOption);
    allOptions.add(indentOption);
    allOptions.add(minListSizeOption);
    allOptions.add(cacheOption);
    allOptions.add(incrementalOption);

    // New since 2.1.11.
    allOptions.add(dotOption);
    allOptions.add(ASTNodeSuperOption);
    allOptions.add(generateImplicitsOption);

    // New since 2.1.12.
    allOptions.add(stateClassNameOption);

    // New since 2.2.1:
    allOptions.add(safeLazyOption);

    // New since 2.2.3:
    allOptions.add(statisticsOption);

    // New since 2.2.4:
    allOptions.add(emptyContainerSingletons);
    allOptions.add(concurrentOption);
    allOptions.add(numThreadsOption);
    allOptions.add(concurrentMap);

    // New since 2.3.4
    allOptions.add(optimizeImports);

    return allOptions;
  }

}
