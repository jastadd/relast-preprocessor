package org.jastadd.relast.compiler;

import org.jastadd.PreprocessorConfiguration;
import org.jastadd.option.FlagOption;
import org.jastadd.option.Option;

import java.util.ArrayList;
import java.util.MissingResourceException;
import java.util.ResourceBundle;

public abstract class AbstractCompiler {

  private final boolean jastAddCompliant;
  private final String name;
  protected ArrayList<Option<?>> options;
  private PreprocessorConfiguration configuration;

  protected AbstractCompiler(String name, boolean jastaddCompliant) {
    this.name = name;
    this.jastAddCompliant = jastaddCompliant;
  }

  public PreprocessorConfiguration getConfiguration() {
    return configuration;
  }

  public int run(String[] args) throws CompilerException {

    options = new ArrayList<>();
    initOptions();
    configuration = new PreprocessorConfiguration(args, System.err, jastAddCompliant, options);

    if (configuration.shouldPrintHelp()) {
      configuration.printHelp(System.out);
      return 0;
    }

    if (configuration.shouldPrintVersion()) {
      try {
        ResourceBundle resources = ResourceBundle.getBundle("preprocessor");
        System.out.println(getName() + ", version " + resources.getString("version"));
      } catch (MissingResourceException e) {
        System.out.println(getName() + ", unknown version");
      }
      return 0;
    }

    return compile();
  }

  protected abstract int compile() throws CompilerException;

  protected void initOptions() {
    if (!jastAddCompliant) {
      addOption(new FlagOption("version", "print version info"));
      addOption(new FlagOption("help", "print command-line usage info"));
    }
  }

  protected <O extends Option<?>> O addOption(O option) {
    options.add(option);
    return option;
  }

  protected int error(String message) {
    System.err.println("Error: " + message);
    System.err.println();
    System.err.println("Usage: java -jar " + name + ".jar [--option1] [--option2=value] ...  <filename1> <filename2> ... ");
    System.err.println("Options:");
    configuration.printHelp(System.err);
    return 1;
  }

  public String getName() {
    return name;
  }
}

