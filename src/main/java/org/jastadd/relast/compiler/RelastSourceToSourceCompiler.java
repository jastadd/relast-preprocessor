package org.jastadd.relast.compiler;

import org.jastadd.relast.ast.GrammarFile;
import org.jastadd.relast.ast.Program;

import java.nio.file.Path;
import java.nio.file.Paths;

public class RelastSourceToSourceCompiler extends RelAstProcessor {


  public RelastSourceToSourceCompiler(String name, boolean jastAddCompliant) {
    super(name, jastAddCompliant);
  }

  public static void main(String[] args) {
    try {
      new RelastSourceToSourceCompiler("Relational RAGs Source-To-Source Compiler", false).run(args);
    } catch (CompilerException e) {
      System.err.println(e.getMessage());
      System.exit(-1);
    }
  }

  @Override
  protected int processGrammar(Program program, Path inputBasePath, Path outputBasePath) throws CompilerException {

    printMessage("Writing output files");

    for (GrammarFile grammarFile : program.getGrammarFileList()) {
      printMessage("Writing output file " + grammarFile.getFileName());
      // TODO decide and document what the file name should be, the full path or a simple name?
      writeToFile(outputBasePath.resolve(grammarFile.getFileName()), grammarFile.generateAbstractGrammar());
    }
    return 0;
  }
}

