package org.jastadd.relast.compiler;

import org.jastadd.option.ValueOption;
import org.jastadd.relast.ast.GrammarFile;
import org.jastadd.relast.ast.Program;
import org.jastadd.relast.parser.RelAstParser;
import org.jastadd.relast.scanner.RelAstScanner;

import java.io.BufferedReader;
import java.io.IOException;
import java.io.PrintWriter;
import java.nio.file.Files;
import java.nio.file.Path;
import java.nio.file.Paths;
import java.util.ArrayList;
import java.util.Collection;
import java.util.Optional;

public abstract class RelAstProcessor extends AbstractCompiler {

  protected ValueOption optionOutputBaseDir;
  protected ValueOption optionInputBaseDir;

  protected RelAstProcessor(String name, boolean jastAddCompliant) {
    super(name, jastAddCompliant);
  }

  protected boolean isGrammarFile(Path path) {
    if (path.getFileName() == null) { return false; }
    String fileName = path.getFileName().toString();
    int dotIndex = fileName.lastIndexOf('.');
    if (dotIndex < 0) {
      printMessage(path + " has no extension, ignoring it.");
      return false;
    }
    String extension = fileName.subSequence(dotIndex, fileName.length()).toString();
    boolean isGrammar = extension.equals(".relast") || extension.equals(".ast");
    if (!isGrammar) {
      printMessage(path + " is not a grammar file, ignoring it.");
    }
    return isGrammar;
  }

  @Override
  protected void initOptions() {
    optionOutputBaseDir = addOption(new ValueOption("outputBaseDir", "base directory for generated files"));
    optionInputBaseDir = addOption(new ValueOption("inputBaseDir", "base directory for input files"));
    super.initOptions();
  }

  @Override
  protected int compile() throws CompilerException {
    final Path inputBasePath;
    if (optionInputBaseDir.isMatched()) {
      inputBasePath = Paths.get(optionInputBaseDir.value()).toAbsolutePath();
    } else {
      inputBasePath = Paths.get(".").toAbsolutePath();
      printMessage("No input base dir is set. Assuming current directory '" + inputBasePath.toAbsolutePath() + "'.");
    }

    if (!inputBasePath.toFile().exists()) {
      printMessage("Input path '" + inputBasePath.toAbsolutePath() + "' does not exist. Exiting...");
      System.exit(-1);
    } else if (!inputBasePath.toFile().isDirectory()) {
      printMessage("Input path '" + inputBasePath.toAbsolutePath() + "' is not a directory. Exiting...");
      System.exit(-1);
    }

    final Path outputBasePath;
    if (optionOutputBaseDir.isMatched()) {
      outputBasePath = Paths.get(optionOutputBaseDir.value()).toAbsolutePath();
    } else {
      throw new CompilerException("No output base dir is set.");
    }

    if (outputBasePath.toFile().exists() && !outputBasePath.toFile().isDirectory()) {
      printMessage("Output path '" + inputBasePath.toAbsolutePath() + "' exists, but is not a directory. Exiting...");
    }

    printMessage("Running " + getName());

    // gather all files
    Collection<Path> inputFiles = new ArrayList<>();
    getConfiguration().getFiles().forEach(name -> checkFileName(inputBasePath, Paths.get(name)).ifPresent(inputFiles::add));


    Program program = parseProgram(inputBasePath, inputFiles);

    return processGrammar(program, inputBasePath, outputBasePath);
  }

  protected abstract int processGrammar(Program program, Path inputBasePath, Path outputBasePath) throws CompilerException;

  private Optional<Path> checkFileName(Path inputBasePath, Path filePath) {
    if (filePath.isAbsolute()) {
      if (filePath.normalize().startsWith(inputBasePath.normalize())) {
        return Optional.of(filePath);
      } else {
        printMessage("Path '" + filePath + "' is not contained in the base path '" + inputBasePath + "', ignoring it.");
        return Optional.empty();
      }
    } else {
      return Optional.of(inputBasePath.resolve(filePath));
    }
  }

  protected void printMessage(String message) {
    System.out.println(message);
  }

  protected void writeToFile(Path path, String str) throws CompilerException {
    //noinspection ResultOfMethodCallIgnored
    path.getParent().toFile().mkdirs(); // create directory structure if necessary
    try (PrintWriter writer = new PrintWriter(path.toFile())) {
      writer.print(str);
    } catch (Exception e) {
      throw new CompilerException("Could not write to file " + path, e);
    }
  }

  private Program parseProgram(Path inputBasePath, Collection<Path> inputFiles) {
    Program program = new Program();

    RelAstParser parser = new RelAstParser();

    inputFiles.stream().filter(this::isGrammarFile).forEach(
        path -> {
          try (BufferedReader reader = Files.newBufferedReader(path)) {
            RelAstScanner scanner = new RelAstScanner(reader);
            GrammarFile inputGrammar = (GrammarFile) parser.parse(scanner);
            inputGrammar.setFileName(inputBasePath.relativize(path).toString());
            program.addGrammarFile(inputGrammar);
          } catch (IOException | beaver.Parser.Exception e) {
            printMessage("Could not parse grammar file " + path);
            e.printStackTrace();
          }
        }
    );

    program.treeResolveAll();

    return program;
  }
}

