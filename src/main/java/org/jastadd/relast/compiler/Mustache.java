package org.jastadd.relast.compiler;

import com.github.jknack.handlebars.Handlebars;
import com.github.jknack.handlebars.Template;
import com.github.jknack.handlebars.io.ClassPathTemplateLoader;
import com.github.jknack.handlebars.io.TemplateLoader;
import org.yaml.snakeyaml.Yaml;

import java.io.*;
import java.nio.file.Paths;

public class Mustache {

  private Mustache() {
    // hide public constructor
  }

  public static void javaMustache(String templateFileName, File yamlFile, String outputFileName) throws IOException {

    //noinspection ResultOfMethodCallIgnored
    Paths.get(outputFileName).getParent().toFile().mkdirs(); // create directory structure if necessary

    Object context = new Yaml().load(new FileReader(yamlFile));
    applyTemplate(templateFileName, outputFileName, context);
  }

  public static void javaMustache(String templateFileName, String yaml, String outputFileName) throws IOException {

    //noinspection ResultOfMethodCallIgnored
    Paths.get(outputFileName).getParent().toFile().mkdirs(); // create directory structure if necessary

    Object context = new Yaml().load(new StringReader(yaml));
    applyTemplate(templateFileName, outputFileName, context);
  }

  private static void applyTemplate(String templateFileName, String outputFileName, Object context) throws IOException {
    TemplateLoader loader = new ClassPathTemplateLoader();
    loader.setSuffix(".mustache"); // the default is ".hbs"

    Handlebars handlebars = new Handlebars(loader);
    handlebars.prettyPrint(true); // set handlebars to mustache mode (skip some whitespace)
    handlebars.infiniteLoops(true); // allow partial recursion
    Template template = handlebars.compile(templateFileName);

    try (Writer w = new FileWriter(outputFileName)) {
      System.out.println("Writing " + outputFileName);
      template.apply(context, w);
      w.flush();
    }
  }

}
