
%%

<DECLARATION> {
  {WhiteSpace}          { /* ignore */ }
  {MultiLineComment}    { /* ignore */ }
  {DocComment}          { /* ignore */ }
  {SingleLineComment}   { /* ignore */ }
}

<YYINITIAL,COMMENT> {
  {WhiteSpace}+         { yybegin(YYINITIAL); return sym(Terminals.WHITESPACE); }
  {MultiLineComment}    { yybegin(YYINITIAL); return sym(Terminals.MULTILINECOMMENT); }
  {DocComment}          { yybegin(YYINITIAL); return sym(Terminals.DOCCOMMENT); }
  {SingleLineComment}   { yybegin(YYINITIAL); return sym(Terminals.SINGLELINECOMMENT); }
}
