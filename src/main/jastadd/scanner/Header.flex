package org.jastadd.relast.scanner;

import org.jastadd.relast.parser.RelAstParser.Terminals;
%%

%public
%final
%class RelAstScanner
%extends beaver.Scanner

%type beaver.Symbol
%function nextToken
%yylexthrow beaver.Scanner.Exception
%scanerror RelAstScanner.ScannerError

%x COMMENT
%s DECLARATION

%line
%column
