<YYINITIAL,COMMENT,DECLARATION> {
  {ID}                  { yybegin(DECLARATION); return sym(Terminals.ID); }
}
<YYINITIAL,COMMENT,DECLARATION> {
  <<EOF>>               { return sym(Terminals.EOF); }
  [^]                   { throw new ScannerError((yyline+1) +"," + (yycolumn+1) + ": Illegal character <"+yytext()+">"); }
}