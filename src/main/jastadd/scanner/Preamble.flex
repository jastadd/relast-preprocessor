%{
  private StringBuilder stringLitSb = new StringBuilder();

  private beaver.Symbol sym(short id) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), yytext());
  }

  private beaver.Symbol sym(short id, String text) {
    return new beaver.Symbol(id, yyline + 1, yycolumn + 1, yylength(), text);
  }


  public static class ScannerError extends Error {
    public ScannerError(String message) {
      super(message);
    }
  }
%}
