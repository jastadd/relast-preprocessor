<YYINITIAL,COMMENT,DECLARATION> {
  ";"                   { yybegin(COMMENT);     return sym(Terminals.SCOL); }
  ":"                   { yybegin(DECLARATION); return sym(Terminals.COL); }
  "::="                 { yybegin(DECLARATION); return sym(Terminals.ASSIGN); }
  "*"                   { yybegin(DECLARATION); return sym(Terminals.STAR); }
  "."                   { yybegin(DECLARATION); return sym(Terminals.DOT); }
  ","                   { yybegin(DECLARATION); return sym(Terminals.COMMA); }
  "<"                   { yybegin(DECLARATION); return sym(Terminals.LT); }
  ">"                   { yybegin(DECLARATION); return sym(Terminals.GT); }
  "["                   { yybegin(DECLARATION); return sym(Terminals.LBRACKET); }
  "]"                   { yybegin(DECLARATION); return sym(Terminals.RBRACKET); }
  "/"                   { yybegin(DECLARATION); return sym(Terminals.SLASH); }
  "?"                   { yybegin(DECLARATION); return sym(Terminals.QUESTION_MARK); }
  "->"                  { yybegin(DECLARATION); return sym(Terminals.RIGHT); }
  "<-"                  { yybegin(DECLARATION); return sym(Terminals.LEFT); }
  "<->"                 { yybegin(DECLARATION); return sym(Terminals.BIDIRECTIONAL); }
}
