WhiteSpace        = [ ] | \t | \f | \n | \r | \r\n
ID                = [a-zA-Z$_][a-zA-Z0-9$_]*
MultiLineComment  = [/][*][^*]+[*]+([^*/][^*]*[*]+)*[/]
DocComment        = [/][*][*][^*]*[*]+([^*/][^*]*[*]+)*[/]
SingleLineComment = [/][/] [^\n\r]* (\n | \r | \r\n)
